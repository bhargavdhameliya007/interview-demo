import './App.css';
import React ,{useState,useEffect} from 'react';
import parse from 'html-react-parser';
const App=()=> {
  const [state, setstate] = useState('');
  const [stateHtml, setHtmlstate] = useState('');
  const [SelectedTextData, setSelectedText] = useState({
    text:'',start:null,end:null
  });

  const onChangeTextData=(data)=>{
    setstate(data)
    setSelectedText({ text:'',start:null,end:null})
    console.log(data)
  }

  function addStr(str, index, stringToAdd){
    return str.substring(0, index) + stringToAdd + str.substring(index, str.length);
  }

  const Bold=()=>{
    console.log('SelectedTextData',SelectedTextData)
    if(SelectedTextData.start!==null&&SelectedTextData.text!==''){
      const a= addStr(state, SelectedTextData.start, '<b>')
      const b= addStr(a, SelectedTextData.end+3, '</b>')
      console.log('a',b);
      setHtmlstate(b)
       setSelectedText({ text:'',start:null,end:null})
      setstate(b)
    }
    else{
      alert('select Data')
    }
  }

  const Italic=()=>{
    console.log('SelectedTextData',SelectedTextData)
    if(SelectedTextData.start!==null&&SelectedTextData.text!==''){
      const a= addStr(state, SelectedTextData.start, '<i>')
      const b= addStr(a, SelectedTextData.end+3, '</i>')
      console.log('a',b);
      setHtmlstate(b)
       setSelectedText({ text:'',start:null,end:null})
      setstate(b)
    }
    else{
      alert('select Data')
    }
  }
  const Underline=()=>{
    console.log('SelectedTextData',SelectedTextData)
    if(SelectedTextData.start!==null&&SelectedTextData.text!==''){
      const a= addStr(state, SelectedTextData.start, '<u>')
      const b= addStr(a, SelectedTextData.end+3, '</u>')
      console.log('a',b);
      setHtmlstate(b)
       setSelectedText({ text:'',start:null,end:null})
      setstate(b)
    }
    else{
      alert('select Data')
    }
  }

  const uppercase=()=>{
    console.log('SelectedTextData',SelectedTextData)
    if(SelectedTextData.start!==null&&SelectedTextData.text!==''){
      const a= addStr(state, SelectedTextData.start, '<p class="upercase">')
      const b= addStr(a, SelectedTextData.end+20, '</p>')
      console.log('a',b);
      setHtmlstate(b)
       setSelectedText({ text:'',start:null,end:null})
      setstate(b)
    }
    else{
      alert('select Data')
    }
  }

  const Heading=()=>{
    console.log('SelectedTextData',SelectedTextData)
    if(SelectedTextData.start!==null&&SelectedTextData.text!==''){
      const a= addStr(state, SelectedTextData.start, '<H1>')
      const b= addStr(a, SelectedTextData.end+20, '</H1>')
      console.log('a',b);
      setHtmlstate(b)
       setSelectedText({ text:'',start:null,end:null})
      setstate(b)
    }
    else{
      alert('select Data')
    }
  }
  const lowercase=()=>{
    console.log('SelectedTextData',SelectedTextData)
    if(SelectedTextData.start!==null&&SelectedTextData.text!==''){
      const a= addStr(state, SelectedTextData.start, '<span class="lowercase">')
      const b= addStr(a, SelectedTextData.end+24, '</span>')
      console.log('a',b);
      setHtmlstate(b)
       setSelectedText({ text:'',start:null,end:null})
      setstate(b)
    }
    else{
      alert('select Data')
    }
  }

  const View=()=>{
    const data =state;
   let a= data.replaceAll('#img=',`<img class='imagecss' src="`)
   let b= a.replaceAll('#img2',`" alt="" />`)
    setHtmlstate(b)
    console.log('asdasd',b)
  }

const mySelection = function (element) {
    let startPos = element.selectionStart;
    let endPos = element.selectionEnd;
    let selectedText = element.value.substring(startPos, endPos);
    if(selectedText.length <= 0) {
      return; // stop here if selection length is <= 0
    }
    setSelectedText({ text:selectedText,start:startPos,end:endPos})
  };
 
const textAreaElements = document.querySelectorAll('textarea');
  [...textAreaElements].forEach(function(element) {
      element.addEventListener('mouseup', function(){
          mySelection(element)
          console.log(element)
  
      });
      element.addEventListener('keyup', function( event ) {
          if(event.keyCode === 16 || event.keyCode === 17 || event.metaKey) {
              mySelection(element)
              console.log(element)
          }
      });
  });

  return (
    <div className="App">
      <div>
        <div className='mainBtnDiv'>
          <div className='btnCSS' onClick={()=>Bold()}>
            Bold
          </div>
          <div className='btnCSS' onClick={()=>{Italic()}}>
          Italic
          </div>
          <div className='btnCSS'  onClick={()=>{Underline()}}>
          Underline
          </div>
          <div className='btnCSS' onClick={()=>{uppercase()}}>
          uppercase 
          </div>
          <div className='btnCSS' onClick={()=>{lowercase()}}>
          lowercase
          </div>
          <div className='btnCSS' onClick={()=>{Heading()}}>
          Heading
          </div>
          <div className='btnCSS' onClick={()=>{View()}}>
            View
          </div>
        </div>
        <textarea className='TextArea' value={state} onChange={(e)=>{onChangeTextData(e.target.value)}}/>
      </div>
    {parse(stateHtml)}
    </div>

  );
}

export default App;
